apgdiff (2.5.0~alpha.2-75-gcaaaed9-2) UNRELEASED; urgency=medium

  * Move packaging repository to salsa.debian.org

 -- Christoph Berg <myon@debian.org>  Mon, 25 Dec 2017 18:03:35 +0100

apgdiff (2.5.0~alpha.2-75-gcaaaed9-1) unstable; urgency=medium

  * New upstream snapshot.
  * Remove build.xml patch, not needed anymore.
  * Update to DH 9.
  * Modernize debian/rules.
  * Add simple autopkgtest.

 -- Christoph Berg <myon@debian.org>  Mon, 23 Oct 2017 18:22:24 +0200

apgdiff (2.4-3) unstable; urgency=medium

  * Set team as Maintainer.
  * Move packaging to git.

 -- Christoph Berg <christoph.berg@credativ.de>  Wed, 02 Jul 2014 11:02:50 +0200

apgdiff (2.4-2) unstable; urgency=low

  * Update watch file to point to http://www.apgdiff.com/download.php.
  * build.xml: Set fork=true to fix java.lang.NoClassDefFoundError:
    junit/framework/JUnit4TestAdapterCache.

 -- Christoph Berg <myon@debian.org>  Tue, 16 Jul 2013 10:11:55 +0200

apgdiff (2.4-1) experimental; urgency=low

  * New upstream version.
    + Supports IF NOT EXISTS in CREATE TABLE. (Closes: #671395)
  * Use 3.0 (quilt).
  * Use README.md as upstream changelog file.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Sep 2012 11:24:01 +0200

apgdiff (2.3-1) unstable; urgency=low

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Sun, 31 Oct 2010 19:45:57 +0100

apgdiff (2.2.2-1) unstable; urgency=low

  * New upstream version.
  * Using changelog included in zipfile, thanks Miroslav for providing this.
  * Update manpage.

 -- Christoph Berg <myon@debian.org>  Mon, 11 Oct 2010 09:08:18 +0200

apgdiff (2.2-1) unstable; urgency=low

  * New upstream version.
  * Update homepage location.
  * Finally enable test suite, yay!

 -- Christoph Berg <myon@debian.org>  Sat, 02 Oct 2010 19:35:17 +0200

apgdiff (1.4-2) unstable; urgency=low

  * Change java runtime depends to default-jre-headless |
    java2-runtime-headless. (Closes: #573377)

 -- Christoph Berg <myon@debian.org>  Thu, 11 Mar 2010 10:00:37 +0100

apgdiff (1.4-1) unstable; urgency=low

  * New upstream version. (Without changelog, I am afraid.)
  * Test suite still not enabled as it needs network access. (The junit
    version in Debian needs upgrading.)
  * Optimize rules files a bit so debhelper doesn't clean twice, and quilt's
    patch target doesn't prevent build-stamp from working.

 -- Christoph Berg <myon@debian.org>  Wed, 02 Dec 2009 11:11:33 +0100

apgdiff (1.3-3) unstable; urgency=low

  * Build-Depend on default-jdk. (Closes: #543055)
  * Use Section: database.
  * Thanks to Michael Koch for packaging tips.

 -- Christoph Berg <myon@debian.org>  Fri, 28 Aug 2009 21:28:17 +0200

apgdiff (1.3-2) unstable; urgency=low

  * Add asciidoc to Build-Depends. (Closes: #533919)
  * Also fix warnings about deprecated glossary syntax in apgdiff.1.txt, and
    add libxml2-utils to Build-Depends. By Andres Rodriguez.
    (Closes: #534395)

 -- Christoph Berg <myon@debian.org>  Thu, 25 Jun 2009 11:41:38 +0200

apgdiff (1.3-1) unstable; urgency=low

  * New upstream version.
  * Manually copy changelog entries from sourceforge.net to CHANGES.
  * Use quilt.
  * Add watch file.

 -- Christoph Berg <myon@debian.org>  Mon, 04 May 2009 11:24:40 +0200

apgdiff (1.2-1) unstable; urgency=low

  * Initial release.

 -- Christoph Berg <myon@debian.org>  Tue, 09 Sep 2008 15:42:54 +0200
